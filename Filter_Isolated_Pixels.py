import os

os.environ["OPENCV_IO_ENABLE_OPENEXR"] = "1"
import json
import cv2
from pathlib import Path
from concurrent.futures import ProcessPoolExecutor
import numpy as np
import json
import math

directory = "D:/Manu/renders/UrbanSyns/UrbanSyns_BB2D/poblenou_terrain/instances"
files = []
for filename in os.listdir(directory):
    # Check if the file is an image
    if filename.endswith(".png"):
        files.append(filename)
def filter(image_param):
    print(image_param)
    # Initialize variables
    color_array = []
    class_dict = {}
    id_array = {}
    color_and_percentage_dict = {}
    image_path = os.path.join(directory, image_param)


    # Open image
    image = cv2.imread(image_path)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    thresh = cv2.threshold(gray, 0, 255,cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
    # Using cv2.imshow() method

    # Displaying the image
    cv2.imshow("window_nam", gray)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    # Displaying the image
    cv2.imshow("window_nam", thresh)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    output = cv2.connectedComponentsWithStats(thresh, 4, cv2.CV_32S)
    (numLabels, labels, stats, centroids) = output
    print(numLabels)
    # loop over the number of unique connected component labels
    for i in range(0, numLabels):
        # if this is the first component then we examine the
        # *background* (typically we would just ignore this
        # component in our loop)
        if i == 0:
            text = "examining component {}/{} (background)".format(
                i + 1, numLabels)
        # otherwise, we are examining an actual connected component
        else:
            text = "examining component {}/{}".format(i + 1, numLabels)
        # print a status message update for the current connected
        # component
        print("[INFO] {}".format(text))
        # extract the connected component statistics and centroid for
        # the current label
        x = stats[i, cv2.CC_STAT_LEFT]
        y = stats[i, cv2.CC_STAT_TOP]
        w = stats[i, cv2.CC_STAT_WIDTH]
        h = stats[i, cv2.CC_STAT_HEIGHT]
        area = stats[i, cv2.CC_STAT_AREA]
        (cX, cY) = centroids[i]

def main():
    filter(files[0])

    # Create a ProcessPoolExecutor with a maximum of 8 threads
    #with ProcessPoolExecutor(max_workers=12) as executor:
     #   executor.map(filter, files)


if __name__ == '__main__':
    main()
