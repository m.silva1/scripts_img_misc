import os
import time
from concurrent.futures import ProcessPoolExecutor
from PIL import Image
import random

# Specify the directory containing the images
directory = "A:/users/cgl/workspace/datasets/Mappilary/training/v1.2/labels"

# Measure the time it takes to load and render the image
start_time = time.perf_counter()
files = []

for filename in os.listdir(directory):
    # Check if the file is an image
    if filename.endswith(".png"):
        image_path = os.path.join(directory, filename)
        image_path = image_path.replace("\\","/")
        files.append(image_path)

def replace_colors(image_param):

    # Get the full path of the image
    #image_path = os.path.join(directory, image_param)
    # Open the image
    image = Image.open(image_param)

    # Convert the image to a list of pixels
    image = image.convert('RGB')
    pixels = list(image.getdata())
    new_pixels = []
    guardrailflag = False
    buildingflag = False
    pedestrianflag = False

    for pixel in pixels:
        match pixel:
            # Car
            case (32, 32, 32):
                guardrailflag = True
            case (70, 70, 70):
                buildingflag = True
            case (220, 20, 60):
                pedestrianflag = True


    if(not guardrailflag):
        os.remove(image_param)
        image_param_rgb = image_param.replace("png", "jpg")
        os.remove(image_param_rgb.replace("v1.2/labels", "images"))

    else:
        print(image_param)

def main():
    # Create a ProcessPoolExecutor with a maximum of 8 threads
    with ProcessPoolExecutor(max_workers=16) as executor:
        set(executor.map(replace_colors, files))

if __name__ == '__main__':
    main()


