import os
import time
from concurrent.futures import ProcessPoolExecutor
from PIL import Image
import json
import shutil
from pathlib import Path
import random
index = 0
# Specify the directory containing the images
directory = "A:/users/cgl/workspace/datasets/Bdd/labels/bdd100k_labels_images_val.json"
directory_images = "A:/users/cgl/workspace/datasets/Bdd/images/10k/val"
directory_images_target = "A:/users/cgl/workspace/datasets/Bdd_filtered/images/10k/val"
path=Path(directory_images_target)
path.mkdir(parents=True, exist_ok=True)
directory_labels = "A:/users/cgl/workspace/datasets/Bdd/labels/sem_seg/colormaps/val"
directory_labels_target = "A:/users/cgl/workspace/datasets/Bdd_filtered/labels/sem_seg/colormaps/val"
path_labels=Path(directory_labels_target)
path_labels.mkdir(parents=True, exist_ok=True)

# Measure the time it takes to load and render the image
start_time = time.perf_counter()
files = []



# Open the JSON
print("Opening")
data = json.load(open(directory, 'r'))
print("Opened")

for i in data:
    if i['attributes']['scene'] == 'highway' and i['attributes']['timeofday'] == 'daytime':
        personflag = False
        for j in i["labels"]:
            if j['category'] == 'person' or j['category'] == 'crosswalk':
                personflag = True
        if not personflag:
            image_path = os.path.join(directory_images, i['name'])
            image_path_target = os.path.join(directory_images_target, i['name'])
            image_path_labels = os.path.join(directory_labels, i['name'])
            image_path_labels = image_path_labels.replace(".jpg", ".png")
            image_path_labels_target = os.path.join(directory_labels_target, i['name'])
            image_path_labels_target = image_path_labels_target.replace(".jpg", ".png")
            if (os.path.isfile(image_path)):
                shutil.copyfile(image_path, image_path_target)
            if (os.path.isfile(image_path_labels)):
                shutil.copyfile(image_path_labels, image_path_labels_target)
            else:
                print("fail")
                print(image_path_labels)




