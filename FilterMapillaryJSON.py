import os
import time
from concurrent.futures import ProcessPoolExecutor
from PIL import Image
import json
import shutil
from pathlib import Path
import random

# Specify the directory containing the images

directory = "C:/Users/cgl/Documents/datasets/real/Mappilary/validation/v2.0/polygons"

directory_new_labels = directory.replace("Mappilary", "Mappilary_filterednew")
directory_new_labels = directory_new_labels.replace("polygons", "labels")
path_labels = Path(directory_new_labels)
path_labels.mkdir(parents=True, exist_ok=True)

directory_new_rgb = directory_new_labels.replace(".png", ".jpg")
directory_new_rgb = directory_new_rgb.replace("v2.0/labels", "images")
path_rgb = Path(directory_new_rgb)
path_rgb.mkdir(parents=True, exist_ok=True)

# Measure the time it takes to load and render the image
start_time = time.perf_counter()
files = []

for filename in os.listdir(directory):
    # Check if the file is an image
    if filename.endswith(".json"):
        image_path = os.path.join(directory, filename)
        image_path = image_path.replace("\\","/")
        files.append(image_path)

def replace_colors(json_param):

    # Open the JSON
    data = json.load(open(json_param, 'r'))

    # Convert the image to a list of pixels

    new_pixels = []
    guardrailflag = False
    buildingflag = False
    pedestrianflag = False

    for i in data["objects"]:
        if i["label"] == 'human--person--person-group' or i["label"] == 'human--person--individual':
            pedestrianflag = True

        if i["label"] == 'construction--barrier--guard-rail':
            guardrailflag = True

    if(not pedestrianflag and guardrailflag):
        image_param = json_param.replace(".json", ".png")
        image_param = image_param.replace("polygons", "labels")
        image_param_rgb = image_param.replace(".png", ".jpg")
        image_param_rgb = image_param_rgb.replace("v2.0/labels", "images")
        image_path_target = image_param_rgb.replace("Mappilary", "Mappilary_filterednew")
        image_path_labels_target = image_param.replace("Mappilary", "Mappilary_filterednew")
        if os.path.isfile(image_path):
            shutil.copyfile(image_path, image_path_labels_target)
        if os.path.isfile(image_param_rgb):
            shutil.copyfile(image_param_rgb, image_path_target)

def main():
    # Create a ProcessPoolExecutor with a maximum of 8 threads
    with ProcessPoolExecutor(max_workers=1) as executor:
        set(executor.map(replace_colors, files))

if __name__ == '__main__':
    main()


