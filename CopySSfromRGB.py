import argparse
import json
import random
import os
from pathlib import Path
import shutil

directory_labels = "C:/Users/cgl/Documents/datasets/real/Mappilary_filterednew/validation/labels"
directory_rgb = "C:/Users/cgl/Documents/datasets/real/Mappilary_filterednew/validation/images_filtered"
directory_labels_new = "C:/Users/cgl/Documents/datasets/real/Mappilary_filterednew/validation/labels_filtered"




for filename in os.listdir(directory_rgb):
    filename = filename.replace("gtFine_color","leftImg8bit")
    filename = filename.replace(".jpg", ".png")
    image_path = directory_labels + "/" + filename
    image_path_target = directory_labels_new + "/" + filename
    print(shutil.copyfile(image_path, image_path_target))