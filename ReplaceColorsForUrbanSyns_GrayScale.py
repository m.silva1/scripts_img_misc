import os
import time
import numpy
from concurrent.futures import ProcessPoolExecutor
from PIL import Image, ImageOps

# Specify the directory containing the images
directory = "A:/users/cgl/workspace/Renders/UrbanSyns/Raw/veteranenstrasse/ss"
directory_out = "A:/users/cgl/workspace/Renders/UrbanSyns/Raw/veteranenstrasse/labels"

# Measure the time it takes to load and render the image
start_time = time.perf_counter()
files = []

for filename in os.listdir(directory):
    # Check if the file is an image
    if filename.endswith(".png"):
        files.append(filename)


def replace_colors(image_param):
    # Get the full path of the image
    image_path = os.path.join(directory, image_param)
    image_path_out = os.path.join(directory_out, image_param)

    # Open the image
    image = Image.open(image_path)

    # Convert the image to a list of pixels
    pixels = list(image.getdata())

    new_pixels = []

    for pixel in pixels:
        match pixel:
            # Void
            case(0, 0, 0):
                new_pixels.append((19, 19, 19))
            # Road
            case(128, 64, 128):
                new_pixels.append((0, 0, 0))
            #  Sidewalk
            case(244, 35, 232):
                new_pixels.append((1, 1, 1))
            #  Building
            case(70, 70, 70):
                new_pixels.append((2, 2, 2))
            #  Wall
            case(102, 102, 156):
                new_pixels.append((3, 3, 3))
            #  Fence
            case(190, 153, 153):
                new_pixels.append((4, 4, 4))
            # Poles
            case (153, 153, 153):
                new_pixels.append((5, 5, 5))
            # Lights
            case(250, 170, 30):
                new_pixels.append((6, 6, 6))
            # Traffic-sign
            case (220, 220, 0):
                new_pixels.append((7, 7, 7))
            # Vegetation
            case (107, 142, 35):
                new_pixels.append((8, 8, 8))
            # Terrain
            case (152, 251, 152):
                new_pixels.append((9, 9, 9))
            # Sky
            case (70, 130, 180):
                new_pixels.append((10, 10, 10))
            # Person
            case (220, 20, 60):
                new_pixels.append((11, 11, 11))
            # Rider
            case (255, 0, 0):
                new_pixels.append((12, 12, 12))
            # Car
            case (0, 0, 142):
                new_pixels.append((13, 13, 13))
            # Truck
            case (0, 0, 70):
                new_pixels.append((14, 14, 14))
            # Bus
            case (0, 0, 90):
                new_pixels.append((15, 15, 15))
            # Train
            case (0, 80, 100):
                new_pixels.append((16, 16, 16))
            # Motorcycle
            case (0, 0, 230):
                new_pixels.append((17, 17, 17))
            # Bicycle
            case (119, 11, 32):
                new_pixels.append((18, 18, 18))
            # Default
            case _:
                print(pixel)
                new_pixels.append((255, 255, 255))

    # Update the image with the new pixels
    image.putdata(new_pixels)

    #Converting into gray image
    red, green, blue = image.split()

    # Save the modified image
    red.save(image_path_out)

    # Print the elapsed time
    elapsed_time = time.perf_counter() - start_time
    print(f"Rendered {image_param} in {elapsed_time:.2f} seconds")


def main():
    # Create a ProcessPoolExecutor with a maximum of 8 threads
    with ProcessPoolExecutor(max_workers=1) as executor:
        executor.map(replace_colors, files)


if __name__ == '__main__':
    main()

'''    for pixel in pixels:
        match pixel:
            # Terrain
            case (128, 128, 128):
                new_pixels.append((0, 0, 0))
            # Traffic-Light
            case (250, 170, 30):
                new_pixels.append((192, 64, 128))
            # Default
            case _:
                new_pixels.append(pixel)'''
