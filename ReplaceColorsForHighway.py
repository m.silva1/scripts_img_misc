import os
import time
from concurrent.futures import ProcessPoolExecutor
from PIL import Image

# Specify the directory containing the images
directory = "E:\ss_original"
directory_out = "E:\ss_highway"

# Measure the time it takes to load and render the image
start_time = time.perf_counter()
files = []

for filename in os.listdir(directory):
    # Check if the file is an image
    if filename.endswith(".png"):
        files.append(filename)


def replace_colors(image_param):
    # Get the full path of the image
    image_path = os.path.join(directory, image_param)
    image_path_out = os.path.join(directory_out, image_param)

    # Open the image
    image = Image.open(image_path)

    # Convert the image to a list of pixels
    pixels = list(image.getdata())

    new_pixels = []

    for pixel in pixels:
        match pixel:
            #Terrain
            case (128, 128, 128):
                new_pixels.append((0, 0, 0))
            #Traffic-Light
            case (250, 170, 30):
                new_pixels.append((192, 64, 128))
            #Default
            case _:
                new_pixels.append(pixel)

    # Update the image with the new pixels
    image.putdata(new_pixels)

    # Save the modified image
    image.save(image_path_out)

    # Print the elapsed time
    elapsed_time = time.perf_counter() - start_time
    print(f"Rendered {image_param} in {elapsed_time:.2f} seconds")


def main():

    # Create a ProcessPoolExecutor with a maximum of 8 threads
    with ProcessPoolExecutor(max_workers=8) as executor:
        executor.map(replace_colors, files)


if __name__ == '__main__':
    main()
