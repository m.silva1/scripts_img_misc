import os
import time
from concurrent.futures import ProcessPoolExecutor
from PIL import Image, ImageOps

# Specify the directory containing the images
directory = "E:\MANU\Datasets\Highway\A8_Carcedo_12_01\ss\ss_original"
directory_out = "E:\MANU\Datasets\Highway\A8_Carcedo_12_01\ss\ss_synthia"

# Measure the time it takes to load and render the image
start_time = time.perf_counter()
files = []

for filename in os.listdir(directory):
    # Check if the file is an image
    if filename.endswith(".png"):
        files.append(filename)


def replace_colors(image_param):
    # Get the full path of the image
    image_path = os.path.join(directory, image_param)
    image_path_out = os.path.join(directory_out, image_param)

    # Open the image
    image = Image.open(image_path)

    # Convert the image to a list of pixels
    pixels = list(image.getdata())

    new_pixels = []

    for pixel in pixels:
        match pixel:
            # Terrain
            case (128, 128, 128):
                new_pixels.append((9, 9, 9))
            # Vegetation
            case (0, 0, 0):
                new_pixels.append((8, 8, 8))
            # Car
            case (0, 128, 64):
                new_pixels.append((13, 13, 13))
            # Motorcycle
            case (0, 0, 192):
                new_pixels.append((17, 17, 17))
            # Sky
            case (128, 192, 0):
                new_pixels.append((10, 10, 10))
            # Bus
            case (128, 0, 64):
                new_pixels.append((15, 15, 15))
            # Truck
            case (128, 128, 192):
                new_pixels.append((14, 14, 14))
            # Rider
            case (128, 64, 0):
                new_pixels.append((12, 12, 12))
            # Road
            case (64, 128, 0):
                new_pixels.append((0, 0, 0))
            # LaneMarking
            case (0, 192, 0):
                new_pixels.append((0, 0, 0))
            # Poles
            case (192, 192, 0):
                new_pixels.append((5, 5, 5))
            # Guard-Rails
            case (128, 128, 0):
                new_pixels.append((19, 19, 19))
            # Traffic-sign
            case (64, 64, 128):
                new_pixels.append((7, 7, 7))
            # Lights
            case (192, 64, 128):
                new_pixels.append((6, 6, 6))
            # Tunnel
            case(192, 128, 128):
                new_pixels.append((19, 19, 19))
            # Bridge
            case(192, 0, 128):
                new_pixels.append((19, 19, 19))
            # Default
            case _:
                new_pixels.append(pixel)

    # Update the image with the new pixels
    image.putdata(new_pixels)

    #Converting into gray image
    red, green, blue = image.split()

    # Save the modified image
    red.save(image_path_out)

    # Print the elapsed time
    elapsed_time = time.perf_counter() - start_time
    print(f"Rendered {image_param} in {elapsed_time:.2f} seconds")


def main():

    # Create a ProcessPoolExecutor with a maximum of 8 threads
    with ProcessPoolExecutor(max_workers=8) as executor:
        executor.map(replace_colors, files)


if __name__ == '__main__':
    main()

'''    for pixel in pixels:
        match pixel:
            # Terrain
            case (128, 128, 128):
                new_pixels.append((0, 0, 0))
            # Traffic-Light
            case (250, 170, 30):
                new_pixels.append((192, 64, 128))
            # Default
            case _:
                new_pixels.append(pixel)'''
