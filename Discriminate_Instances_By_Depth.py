import os

os.environ["OPENCV_IO_ENABLE_OPENEXR"] = "1"
import json
import cv2
import math

def calculate_depth_distance(depthPixel, meanDepthClass):

    if meanDepthClass - threshold < depthPixel < meanDepthClass + threshold:
        return True
    else:
        return False


directory_instance_id = "A:/users/cgl/workspace/Renders/UrbanSyns/UrbanSyns_BB2D/torstrasse/instances"
directory_depth = "A:/users/cgl/workspace/Renders/UrbanSyns/UrbanSyns_BB2D/torstrasse/depth"

# Initializing variables
pixels_array = []
object_array = {}
pixels_to_change_by_class = {}
class_repeated_array = []
files = []

# Threshold to decide the Z distance to discriminate when a pixel belong to the same car or to another
threshold = 0.00005

# Iterate through the all the instance maps
for filename in os.listdir(directory_instance_id):

    # Check if the file is an image
    if filename.endswith(".png"):

        # Opening Instance Map file
        instance_path = os.path.join(directory_instance_id, filename)
        instance_img = cv2.imread(instance_path)

        # Opening Depth map file (Change paths and read flags from OpenCV)
        filename = filename.replace(".png", ".exr")
        filename = filename.replace("objectcolor", "depth")
        depth_path = os.path.join(directory_depth, filename)
        depth_img = cv2.imread(depth_path, cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)

        # Get the dimensions of the image
        height, width = depth_img.shape

        # Iterating through every pixel of the instance map
        for i in range(0, height):
            for j in range(0, width):  # ...

                # Discarding values that are not associated with instances (i.e: road, sky etc)
                if not ((instance_img[i, j] == [10, 10, 10]).all() or (instance_img[i, j] == [0, 0, 0]).all() or (
                        instance_img[i, j] == [0, 1, 0]).all() or (instance_img[i, j] == [0, 13, 5]).all()):

                    # Converting the values of the pixel in a string index for a dictionary
                    # This is for storing a dictionary with id of the pixel and its Z value
                    idx = str(instance_img[i, j][0]) + str(instance_img[i, j][1])

                    # Checking if the class of the object exists in the dictionary. If that so continue.
                    if idx in object_array:

                        # Check if the Z value of this pixel is in the range for this class, if it is no that means
                        # that belong to another vehicle
                        if not calculate_depth_distance(depth_img[i, j], object_array[idx]):

                            # We store an array with pixels to change the color at the end of the script
                            # This is for ensuring that colors do not repeat
                            if not (idx in pixels_to_change_by_class):
                                pixels_to_change_by_class[idx] = []

                            # Assign new values of the pixels for the new vehicle
                            newValueG = round(instance_img[i, j][2] / 2)
                            newValueR = round(instance_img[i, j][1] / 2)
                            new_color = [instance_img[i, j][0], newValueG, newValueR]

                            # Assign this new value to this pixel that doesn't belong to the main vehicle
                            pixels_to_change_by_class[idx].append([[i, j], new_color])

                    # If the id didn't exist that means that this vehicle is not yet represented in the dictionary,
                    # so we are going to add it
                    else:
                        object_array[idx] = depth_img[i, j]

        # Iterate through the dictionary to change all the pixel color that has to be changed
        for idx in pixels_to_change_by_class:
            for pixel in pixels_to_change_by_class[idx]:
                instance_img[pixel[0][0], pixel[0][1]] = pixel[1]

        #Save the new image
        instance_path_edited = instance_path.replace("instances", "instances_edited")
        # instance_img = cv2.cvtColor(instance_img,cv2.COLOR_BGR2RGB)
        cv2.imwrite(instance_path_edited, instance_img)


