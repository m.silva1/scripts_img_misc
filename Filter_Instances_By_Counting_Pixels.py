import os

os.environ["OPENCV_IO_ENABLE_OPENEXR"] = "1"
import json
import cv2
from pathlib import Path
from concurrent.futures import ProcessPoolExecutor
import numpy as np
import json
import math

directory = "D:/Manu/renders/UrbanSyns/UrbanSyns_BB2D/veteranenstrasse/instances"
directory_ind = "D:/Manu/renders/UrbanSyns/UrbanSyns_BB2D/veteranenstrasse/instances_individual"
directory_depth = "D:/Manu/renders/UrbanSyns/UrbanSyns_BB2D/veteranenstrasse/depth"
directory_out = "D:/Manu/renders/UrbanSyns/UrbanSyns_BB2D/veteranenstrasse/instances_filtered"
directory_json = "D:/Manu/renders/UrbanSyns/UrbanSyns_BB2D/veteranenstrasse/boundingbox2D"
directory_json_edited = "D:/Manu/renders/UrbanSyns/UrbanSyns_BB2D/veteranenstrasse/boundingbox2D_edited"
path_rgb = Path(directory_out)
path_rgb.mkdir(parents=True, exist_ok=True)
path_json = Path(directory_json_edited)
path_json.mkdir(parents=True, exist_ok=True)
files = []

for filename in os.listdir(directory):
    # Check if the file is an image
    if filename.endswith(".png"):
        files.append(filename)
def filter(image_param):
    print(image_param)
    # Initialize variables
    color_and_percentage_np = np.array(11)
    color_array = []
    class_dict = {}
    id_array = {}
    depth_dict = {}
    classes_filtered = []
    color_and_percentage_dict = {}
    image_path = os.path.join(directory, image_param)
    # Opening Depth
    image_path_depth = image_path.replace("objectcolor","depth")
    image_path_depth = image_path_depth.replace("instances","depth")
    image_path_depth = image_path_depth.replace(".png",".exr")

    # Opening JSON file
    image_path_json = image_path.replace(".png", ".json")
    image_path_json = image_path_json.replace("instances", "boundingbox2D")
    image_path_json = image_path_json.replace("objectcolor", "bb2d")
    image_path_json_out = image_path_json.replace("boundingbox2D", "boundingbox2D_edited")
    f = open(image_path_json)

    # returns JSON object as
    # a dictionary
    data = json.load(f)

    # Searching the objects to filter
    # list
    bbox_dict = {}
    for i in data:
        color = i['color'][:-1]
        if i['label'] == 'rider':
            color[2] = 12
        color_array.append(color)
        color_id = str(color[0]) + str(color[1]) + str(color[2])
        bbox = i['bbox']
        bbox_dict[color_id] = bbox

    # Closing file
    f.close()

    # Open image
    image_path_out = os.path.join(directory_out, image_param)
    image = cv2.imread(image_path)
    image_depth = cv2.imread(image_path_depth, cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)
    # Get the dimensions of the image
    height, width, channels = image.shape
    # Iterating through every pixel of the instance map
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    for i in range(0, height):
        for j in range(0, width):
            pixel = image[i][j]
            depth = image_depth[i][j]
            idx = str("{:03}".format(pixel[0])) + str("{:03}".format(pixel[1])) + str("{:03}".format(pixel[2]))
            idx_array = int(idx)
            if not (pixel == [0, 0, 0]).all():
                flag = False
                for color in color_array:
                    if (pixel == color).all():
                        color_id = str(pixel[0]) + str(pixel[1]) + str(pixel[2])
                        bbox = bbox_dict[color_id]
                        if bbox['xMin'] <= j <= bbox['xMax'] and bbox['yMin'] <= i <= bbox['yMax']:
                            flag = True
                            if idx not in class_dict:
                                class_dict[idx] = 0
                            class_dict[idx] = class_dict[idx] + 1
                            id_array[idx_array] = pixel
                if not flag:
                    image[i][j] = [0, 0, 0]
    for key in class_dict:
        # Get unocluded Image
        id = str(id_array[int(key)][0]) + str(id_array[int(key)][1])
        str_replace = "_id_" + id + "_label_" + str(id_array[int(key)][2]) + ".png"
        image_path_ind = image_path.replace(".png", str_replace)
        image_path_ind = image_path_ind.replace("instances", "instances_individual")
        image_ind = cv2.imread(image_path_ind)
        b, g, r = cv2.split(image_ind)
        b_flattened = b.flatten()
        b_count = np.bincount(b_flattened)
        new_array = np.delete(b_count, np.where(b_count == 0))
        max_value = np.amax(new_array)
        new_array = np.delete(new_array, np.where(new_array == max_value))
        # Calculate Oclussion Percentage
        unocluded_object_count = new_array[0]
        ocluded_object_count = class_dict[key]
        oclussion_percentage = 100 - (ocluded_object_count / unocluded_object_count) * 100
        color = []
        color.append(int(key[0] + key[1] + key[2]))
        color.append(int(key[3] + key[4] + key[5]))
        color.append(int(key[6] + key[7] + key[8]))
        color_id = str(color[0]) + str(color[1]) + str(color[2])
        if -2 < oclussion_percentage < 0:
            oclussion_percentage = 0
        color_and_percentage_dict[color_id] = round(oclussion_percentage, 2)


    # Write it in JSON file
    # Opening JSON file
    image_path_json = image_path.replace(".png", ".json")
    image_path_json = image_path_json.replace("instances", "boundingbox2D")
    image_path_json = image_path_json.replace("objectcolor", "bb2d")
    image_path_json_out = image_path_json.replace("boundingbox2D", "boundingbox2D_edited")
    f = open(image_path_json)

    # returns JSON object as
    # a dictionary
    data = json.load(f)
    # Iterating through the json
    # list
    del_array = []
    for count,i in enumerate(data):
        if i['label'] == 'rider':
            i['color'][2] = 12
        color_id = str(i['color'][0]) + str(i['color'][1]) + str(i['color'][2])
        if color_id in color_and_percentage_dict:
            i['occlusion_percentage'] = color_and_percentage_dict[color_id]
        else:
            del_array.insert(0, count)
    for delete in del_array:
        del data[delete]
    # Closing file
    f.close()

    # Serializing json
    json_object = json.dumps(data, indent=4)
    # Writing to sample.json

    with open(image_path_json_out, "w") as outfile:
        outfile.write(json_object)

    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    cv2.imwrite(image_path_out, image)


def main():
    #filter(files[3])

    # Create a ProcessPoolExecutor with a maximum of 8 threads
    with ProcessPoolExecutor(max_workers=18) as executor:
     executor.map(filter, files)


if __name__ == '__main__':
    main()
