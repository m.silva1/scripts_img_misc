import os
os.environ["OPENCV_IO_ENABLE_OPENEXR"]="1"
import cv2

directory = "E:/MANU/Datasets/Colorization/GTA/labels/labels_full"
files = []
for filename in os.listdir(directory):
    # Check if the file is an image
    if filename.endswith(".png"):
        image_path = os.path.join(directory, filename)
        image_path = image_path.replace("\\","/")
        image = cv2.imread(image_path)
        image = cv2.resize(image, (1024, 563), interpolation=cv2.INTER_NEAREST_EXACT)
        image = image[25:537, 0:1024]
        image_path = image_path.replace("labels/labels_full","labels/labels_resize")
        print(image_path)
        cv2.imwrite(image_path,image)

