import os
import time
import numpy
from concurrent.futures import ProcessPoolExecutor
from PIL import Image, ImageOps

# Specify the directory containing the images
directory = "C:/Users/cgl/Documents/datasets/real/Mappilary/training/v1.2/labels"
directory_out = "C:/Users/cgl/Documents/datasets/real/Mappilary/training/v1.2/new_labels"

# Measure the time it takes to load and render the image
start_time = time.perf_counter()
files = []

for filename in os.listdir(directory):
    # Check if the file is an image
    if filename.endswith(".png"):
        files.append(filename)


def replace_colors(image_param):
    # Get the full path of the image
    image_path = os.path.join(directory, image_param)
    image_path_out = os.path.join(directory_out, image_param)

    # Open the image
    image = Image.open(image_path)

    # Convert the image to a list of pixels
    image = image.convert('RGB')
    pixels = list(image.getdata())

    new_pixels = []

    for pixel in pixels:
        match pixel:
            # Bird
            case(165, 42, 42):
                new_pixels.append((19, 19, 19))
            # Ground-Animal
            case(0, 192, 0):
                new_pixels.append((0, 0, 0))
            # Curb
            case(196, 196, 196):
                new_pixels.append((1, 1, 1))
            # Fence
            case(190, 153, 153):
                new_pixels.append((4, 4, 4))
            # Guard-Rail
            case(180, 165, 180):
                new_pixels.append((5, 5, 5))
            # Barrier
            case(90, 120, 150):
                new_pixels.append((19, 19, 19))
            # Wall
            case (102, 102, 156):
                new_pixels.append((3, 3, 3))
            # Bike Lane
            case(128, 64, 255):
                new_pixels.append((0, 0, 0))
            # Crosswalk Plain
            case (140, 140, 200):
                new_pixels.append((0, 0, 0))
            # Curb-Cut
            case (170, 170, 170):
                new_pixels.append((1, 1, 1))
            # Parking
            case (250, 170, 160):
                new_pixels.append((0, 0, 0))
            # Pedestrian Area
            case (96, 96, 96):
                new_pixels.append((1, 1, 1))
            # Rail Track
            case (230, 150, 140):
                new_pixels.append((0, 0, 0))
            # Road
            case (128, 64, 128):
                new_pixels.append((0, 0, 0))
            # Service Lane
            case (110,110, 110):
                new_pixels.append((0, 0, 0))
            # Sidewalk
            case (244, 35, 232):
                new_pixels.append((1, 1, 1))
            # Bridge
            case (150, 100, 100):
                new_pixels.append((3, 3, 3))
            # Building
            case (70, 70, 70):
                new_pixels.append((2, 2, 2))
            # Tunnel
            case (150, 120, 90):
                new_pixels.append((3, 3, 3))
            # Person
            case (220, 20, 60):
                new_pixels.append((11, 11, 11))
            # Bicyclist
            case (255, 0, 0):
                new_pixels.append((12, 12, 12))
            # Motorciclyst
            case (255, 0, 100):
                new_pixels.append((12, 12, 12))
            # Other Rider
            case (255, 0 , 200):
                new_pixels.append((12, 12, 12))
            # Lane Marking -- crosswalk
            case (200, 128, 128):
                new_pixels.append((0, 0, 0))
            # Lane Marking -general
            case (255, 255, 255):
                new_pixels.append((0, 0, 0))
            # Mountain
            case (64, 170, 64):
                new_pixels.append((9, 9, 9))
            # Sand
            case (230, 160, 50):
                new_pixels.append((9, 9, 9))
            # Sky
            case (70, 130, 180):
                new_pixels.append((10, 10, 10))
            # Snow
            case (190, 255, 255):
                new_pixels.append((9, 9, 9))
            # Terrain
            case (152, 251, 152):
                new_pixels.append((10, 10, 10))
            # Vegetation
            case (107, 142, 35):
                new_pixels.append((8, 8, 8))
            # Water
            case (0, 170, 30):
                new_pixels.append((19, 19, 19))
            # Banner
            case (255, 255, 128):
                new_pixels.append((2, 2, 2))
            # Bench
            case (250, 0, 30):
                new_pixels.append((1, 1, 1))
            # Bike Rack
            case (100, 140, 180):
                new_pixels.append((5, 5, 5))
            # Billboard
            case (220, 220, 220):
                new_pixels.append((2, 2, 2))
            # Catch basin
            case (220, 128, 128):
                new_pixels.append((0, 0, 0))
            # CCTV Camera
            case (222, 40, 40):
                new_pixels.append((19, 19, 19))
            # Fire Hydrant
            case (100, 170, 30):
                new_pixels.append((1, 1, 1))
            # Junction Box
            case (40, 40, 40):
                new_pixels.append((19, 19, 19))
            # MailBox
            case (33, 33, 33):
                new_pixels.append((1, 1, 1))
            # ManHole
            case (100, 128, 160):
                new_pixels.append((0, 0, 0))
            # Phone Booth
            case (142, 0, 0):
                new_pixels.append((19, 19, 19))
            # PotHole
            case (70, 100, 150):
                new_pixels.append((0, 0, 0))
            # StreetLight
            case (210, 170, 100):
                new_pixels.append((5, 5, 5))
            # Pole
            case (153, 153, 153):
                new_pixels.append((5, 5, 5))
            # Traffic Sign Frame
            case (128, 128, 128):
                new_pixels.append((7, 7, 7))
            # Utility Pole
            case (0, 0, 80):
                new_pixels.append((5, 5, 5))
            # Traffic Light
            case (250, 170, 30):
                new_pixels.append((6, 6, 6))
            # Traffic Sign Back
            case (192, 192, 192):
                new_pixels.append((7, 7, 7))
            # Traffic Sign Front
            case (220, 220, 0):
                new_pixels.append((7, 7, 7))
            # Trash Can
            case (140, 140, 20):
                new_pixels.append((1, 1, 1))
            # Bicycle
            case (119, 11, 32):
                new_pixels.append((18, 18, 18))
            # Boat
            case (150, 0, 255):
                new_pixels.append((19, 19, 19))
            # Bus
            case (0, 60, 100):
                new_pixels.append((15, 15, 15))
            # Car
            case (0, 0, 142):
                new_pixels.append((13, 13, 13))
            # Caravan
            case (0, 0, 90):
                new_pixels.append((14, 14, 14))
            # Motorcycle
            case (0, 0, 230):
                new_pixels.append((17, 17, 17))
            # On Rails
            case (0, 80, 100):
                new_pixels.append((16, 16, 16))
            # Other Vehicle
            case (128, 64, 64):
                new_pixels.append((19, 19, 19))
            # Trailer
            case (0, 0, 110):
                new_pixels.append((14, 14, 14))
            # Truck
            case (0, 0, 70):
                new_pixels.append((14, 14, 14))
            # Wheeled Slow
            case (0, 0, 192):
                new_pixels.append((19, 19, 19))
            # Car Mount
            case (32, 32, 32):
                new_pixels.append((13, 13, 13))
            # Ego Vehicle
            case (120, 10, 10):
                new_pixels.append((19, 19, 19))
            # Unlabeled
            case (0, 0, 0):
                new_pixels.append((19, 19, 19))
            # Default
            case _:
                print(pixel)
                new_pixels.append((255, 255, 255))

    # Update the image with the new pixels
    image.putdata(new_pixels)

    #Converting into gray image
    red, green, blue = image.split()

    # Save the modified image
    red.save(image_path_out)

    # Print the elapsed time
    elapsed_time = time.perf_counter() - start_time
    print(f"Rendered {image_param} in {elapsed_time:.2f} seconds")


def main():
    # Create a ProcessPoolExecutor with a maximum of 8 threads
    with ProcessPoolExecutor(max_workers=24) as executor:
        executor.map(replace_colors, files)


if __name__ == '__main__':
    main()

'''    for pixel in pixels:
        match pixel:
            # Terrain
            case (128, 128, 128):
                new_pixels.append((0, 0, 0))
            # Traffic-Light
            case (250, 170, 30):
                new_pixels.append((192, 64, 128))
            # Default
            case _:
                new_pixels.append(pixel)'''
