import os

os.environ["OPENCV_IO_ENABLE_OPENEXR"] = "1"
import json
import cv2
from pathlib import Path
from concurrent.futures import ProcessPoolExecutor
import numpy as np
import json
import math

directory_labels = "D:/Manu/renders/UrbanSyns/UrbanSyns_BB2D/veteranenstrasse/labels_fixed"
directory_instances = "D:/Manu/renders/UrbanSyns/UrbanSyns_BB2D/veteranenstrasse/instances_filtered"
directory_instances_out = "D:/Manu/renders/UrbanSyns/UrbanSyns_BB2D/veteranenstrasse/instances_filtered_out"
directory_json = "D:/Manu/renders/UrbanSyns/UrbanSyns_BB2D/veteranenstrasse/boundingbox2D_edited"
directory_json_out = "D:/Manu/renders/UrbanSyns/UrbanSyns_BB2D/veteranenstrasse/boundingbox2D_edited_out"
path_rgb = Path(directory_instances_out)
path_rgb.mkdir(parents=True, exist_ok=True)
path_json = Path(directory_json_out)
path_json.mkdir(parents=True, exist_ok=True)
files = []

for filename in os.listdir(directory_labels):
    # Check if the file is an image
    if filename.endswith(".png"):
        files.append(filename)
def filter(image_param):
    print("Entering")
    print(image_param)
    #Creating variables
    ids_array = []
    # Creating paths
    labels_path = os.path.join(directory_labels, image_param)
    instance_path = os.path.join(directory_instances, image_param)
    instance_path_out = os.path.join(directory_instances_out, image_param)

    # Open image
    image_labels = cv2.imread(labels_path)
    image_instance = cv2.imread(instance_path)

    # Get the dimensions of the image
    height, width, channels = image_labels.shape
    # Iterating through every pixel of the instance map
    image_labels = cv2.cvtColor(image_labels, cv2.COLOR_BGR2RGB)
    image_instance = cv2.cvtColor(image_instance, cv2.COLOR_BGR2RGB)
    for i in range(0, height):
        for j in range(0, width):
            pixel = image_labels[i][j]
            if (pixel == [2, 2, 2]).all():
                color = image_instance[i][j].copy()
                if not (color == [0,0,0]).all():
                    print(color)
                    print("Found error")
                image_instance[i][j] = [0, 0, 0]
                if not ids_array:
                    ids_array.append(color)
                if not np.any(np.all(ids_array == color, axis=1)):
                    ids_array.append(color)

    image_instance = cv2.cvtColor(image_instance, cv2.COLOR_RGB2BGR)
    cv2.imwrite(instance_path_out, image_instance)
    '''
    # Write it in JSON file
    # Opening JSON file
    image_path_json = labels_path.replace(".png", ".json")
    image_path_json = image_path_json.replace("labels", "boundingbox2D")
    image_path_json = image_path_json.replace("objectcolor", "bb2d")
    image_path_json_out = image_path_json.replace("boundingbox2D", "boundingbox2D_edited_out")
    f = open(image_path_json)
    
    # returns JSON object as
    # a dictionary
    data = json.load(f)
    # Iterating through the json
    # list
    del_array = []
    for count,i in enumerate(data):
        for color in ids_array:
            color_json = i['color']
            color_json = np.delete(color_json,3)
            if (color_json == color).all():
                print("Found Intruder")
                del_array.append(count)

    for delete in del_array:
        del data[delete]
    # Closing file
    f.close()

    # Serializing json
    json_object = json.dumps(data, indent=4)
    # Writing to sample.json
    print("Saving")
    print(image_path_json_out)
    with open(image_path_json_out, "w") as outfile:
        outfile.write(json_object)
    '''

def main():
    #filter(files[0])
    for file in files:
        filter(file)
    # Create a ProcessPoolExecutor with a maximum of 8 threads
    #with ProcessPoolExecutor(max_workers=14) as executor:
       #executor.map(filter, files)


if __name__ == '__main__':
    main()
