import os
import time
import numpy
from concurrent.futures import ProcessPoolExecutor
from PIL import Image, ImageOps

# Specify the directory containing the images
directory = "A:/users/cgl/workspace/Renders/UrbanSyns/Raw/poblenou/ss"
directory_out = "A:/users/cgl/workspace/Renders/UrbanSyns/Raw/poblenou/labels"

# Measure the time it takes to load and render the image
start_time = time.perf_counter()
files = []

for filename in os.listdir(directory):
    # Check if the file is an image
    if filename.endswith(".png"):
        files.append(filename)


def replace_colors(image_param):
    # Get the full path of the image
    image_path = os.path.join(directory, image_param)
    image_path_out = os.path.join(directory_out, image_param)

    # Open the image
    image = Image.open(image_path)

    # Convert the image to a list of pixels
    pixels = list(image.getdata())

    new_pixels = []

    for pixel in pixels:
        match pixel:
            # Void
            case(19, 19, 19):
                new_pixels.append((0, 0, 0))
            # Road
            case(0, 0, 0):
                new_pixels.append((128, 64, 128))
            #  Sidewalk
            case(1, 1, 1):
                new_pixels.append((244, 35, 232))
            #  Building
            case(2, 2, 2):
                new_pixels.append((70, 70, 70))
            #  Wall
            case(3, 3, 3):
                new_pixels.append((102, 102, 156))
            #  Fence
            case(4, 4, 4):
                new_pixels.append((190, 153, 153))
            # Poles
            case (5, 5, 5):
                new_pixels.append((153, 153, 153))
            # Lights
            case(6, 6, 6):
                new_pixels.append((250, 170, 30))
            # Traffic-sign
            case (7, 7, 7):
                new_pixels.append((220, 220, 0))
            # Vegetation
            case (8, 8, 8):
                new_pixels.append((107, 142, 35))
            # Terrain
            case (9, 9, 9):
                new_pixels.append((152, 251, 152))
            # Sky
            case (10, 10, 10):
                new_pixels.append((70, 130, 180))
            # Person
            case (11, 11, 11):
                new_pixels.append((220, 20, 60))
            # Rider
            case (12, 12, 12):
                new_pixels.append((255, 0, 0))
            # Car
            case (13, 13, 13):
                new_pixels.append((0, 0, 142))
            # Truck
            case (14, 14, 14):
                new_pixels.append((0, 0, 70))
            # Bus
            case (15, 15, 15):
                new_pixels.append((0, 0, 90))
            # Train
            case (16, 16, 16):
                new_pixels.append((0, 80, 100))
            # Motorcycle
            case (17, 17, 17):
                new_pixels.append((0, 0, 230))
            # Bicycle
            case (18, 18, 18):
                new_pixels.append((119, 11, 32))
            # Default
            case _:
                print(pixel)
                new_pixels.append((255, 255, 255))

    # Update the image with the new pixels
    #image.putdata(new_pixels)

    #Converting into gray image
    red, green, blue = image.split()

    # Save the modified image
    red.save(image_path_out)

    # Print the elapsed time
    elapsed_time = time.perf_counter() - start_time
    #print(f"Rendered {image_param} in {elapsed_time:.2f} seconds")


def main():
    # Create a ProcessPoolExecutor with a maximum of 8 threads
    with ProcessPoolExecutor(max_workers=24) as executor:
        executor.map(replace_colors, files)


if __name__ == '__main__':
    main()

'''    for pixel in pixels:
        match pixel:
            # Terrain
            case (128, 128, 128):
                new_pixels.append((0, 0, 0))
            # Traffic-Light
            case (250, 170, 30):
                new_pixels.append((192, 64, 128))
            # Default
            case _:
                new_pixels.append(pixel)'''
