import os
import time
from concurrent.futures import ProcessPoolExecutor
from PIL import Image
import random

# Specify the directory containing the images
directory = "A:/users/cgl/workspace/Renders/UrbanSyns/2023-06-30-veteranenstrasse_OldLayout/labels"
directory_2 = "A:/users/cgl/workspace/Renders/UrbanSyns/2023-06-30-torstrasse/labels"
directory_3 =  "A:/users/cgl/workspace/Renders/UrbanSyns/2023-06-30-poblenou/poblenou/labels"
directory_4 =  "A:/users/cgl/workspace/Renders/UrbanSyns/2023-06-30-poblenou/poblenou_terrain/labels"

directories = [directory,directory_2,directory_3,directory_4]
#directories = directory
path_to_replace = "A:/users/cgl/workspace/Renders/UrbanSyns/"
new_path = "/data/datasets/new_synthetic_dataset/"

# Measure the time it takes to load and render the image
start_time = time.perf_counter()
files = []

for odd in directories:
    for filename in os.listdir(odd):
        # Check if the file is an image
        if filename.endswith(".png"):
            image_path = os.path.join(odd, filename)
            image_path = image_path.replace("\\","/")
            files.append(image_path)

def replace_colors(image_param):

    # Get the full path of the image
    #image_path = os.path.join(directory, image_param)
    print(image_param)
    # Open the image
    image = Image.open(image_param)

    # Convert the image to a list of pixels
    pixels = list(image.getdata())

    new_pixels = []

    carCount = motorcycleCount = bikeCount = riderCount = pedestrianCount = busCount = trainCount = truckCount = signCount = miscCount = 0

    for pixel in pixels:
        match pixel:
            # Car
            case (13, 13, 13):
                carCount += 1
            # Motorcycle
            case (17, 17, 17):
                motorcycleCount += 1
            # Bicycle
            case (18, 18, 18):
                bikeCount += 1
            # Rider
            case (12, 12, 12):
                riderCount += 1
            # Pedestrian
            case (11, 11, 11):
                pedestrianCount += 1
            # Bus
            case (15, 15, 15):
                busCount += 1
            # Truck
            case (14, 14, 14):
                truckCount += 1
            # Train
            case (16, 16, 16):
                trainCount += 1
            # Traffic-sign
            case (7, 7, 7):
                signCount += 1
            # Default
            case _:
                miscCount += 1

    #Add all the pixels, calculate percentages and filter images with bad class distribution
    total_pixels = carCount + motorcycleCount + bikeCount + riderCount + pedestrianCount + busCount + trainCount + truckCount + signCount + miscCount
    #if ((truckCount / total_pixels) * 100) < 20 and ((trainCount / total_pixels) * 100) < 20 and ((busCount / total_pixels) * 100) < 20 and ((carCount / total_pixels) * 100) < 30 and ((pedestrianCount / total_pixels) * 100) < 25 and ((riderCount / total_pixels) * 100) < 25 and ((signCount / total_pixels) * 100) < 25 and ((motorcycleCount / total_pixels) * 100) < 10 and ((bikeCount / total_pixels) * 100) < 20:
    #print((carCount / total_pixels) * 100)
    if ((truckCount / total_pixels) * 100) < 20 and ((trainCount / total_pixels) * 100) < 20 and ((busCount / total_pixels) * 100) < 20:
        #print(image_param)
        #print("Selected")
        path_ss = image_param.replace("\\", "/")
        path_ss = path_ss.replace(path_to_replace, new_path)
        return path_ss
    else:
        print("Discarded")

def writeTXT(path_list_ss):
    file_rgb = open('A:/users/cgl/workspace/Renders/ours_FullRes_06_26_images_LAB_cityscapes.txt', 'w')
    file_ss = open('A:/users/cgl/workspace/Renders/ours_FullRes_06_26_labels.txt', 'w')

    for path_ss in path_list_ss:
        if path_ss is not None:
            #Write label txt
            file_ss.write(path_ss)
            file_ss.write('\n')
            #Write rgb images txt
            path_rgb = path_ss.replace("labels", "rgb_translated_cityscapes")
            path_rgb = path_rgb.replace("objectcolor", "beauty")
            file_rgb.write(path_rgb)
            file_rgb.write('\n')

    file_ss.close()
    file_rgb.close()

def main():
    # Create a ProcessPoolExecutor with a maximum of 8 threads
    with ProcessPoolExecutor(max_workers=15) as executor:
        path_list_ss = set(executor.map(replace_colors, files))
        print(path_list_ss)
        writeTXT(path_list_ss)

if __name__ == '__main__':
    main()


