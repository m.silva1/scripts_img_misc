import os

os.environ["OPENCV_IO_ENABLE_OPENEXR"] = "1"
import json
import cv2
from pathlib import Path
from concurrent.futures import ProcessPoolExecutor
import math

directory = "D:/Manu/renders/UrbanSyns/UrbanSyns_BB2D/torstrasse/boundingbox2D"
directory_images = "D:/Manu/renders/UrbanSyns/UrbanSyns_BB2D/torstrasse/png"
directory_out = "D:/Manu/renders/UrbanSyns/UrbanSyns_BB2D/poblenou_terrain/boundingbox2D_out"
path_rgb = Path(directory_out)
path_rgb.mkdir(parents=True, exist_ok=True)

files = []
for filename in os.listdir(directory):
    # Check if the file is an image
    if filename.endswith(".json"):
        files.append(filename)


def draw_bb(filename_param):
    # Opening JSON file
    json_path = os.path.join(directory, filename_param)
    file = open(json_path)
    data = json.load(file)

    # Open image
    image_path = json_path.replace("bb2d", "beauty")
    image_path = image_path.replace("boundingbox2D", "png")
    image_path = image_path.replace("json", "png")
    image_path = image_path.replace('\\', "/")
    image_path_out = image_path.replace('png/', "boundingbox2D_out/")
    cv2.waitKey(0)
    print(image_path_out)
    image = cv2.imread(image_path)
    for i in data:
        print(i['bbox'])
        start_point = (i['bbox']['xMin'], i['bbox']['yMin'])
        end_point = (i['bbox']['xMax'], i['bbox']['yMax'])
        # Blue color in BGR
        color = (255, 0, 0)
        # Line thickness of 1 px
        thickness = 1
        new_image = cv2.rectangle(image, start_point, end_point, color, thickness)
        cv2.imwrite(image_path_out, new_image)


def main():
    # Create a ProcessPoolExecutor with a maximum of 8 threads
    with ProcessPoolExecutor(max_workers=10) as executor:
        executor.map(draw_bb, files)


if __name__ == '__main__':
    main()
