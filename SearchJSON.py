import os

os.environ["OPENCV_IO_ENABLE_OPENEXR"] = "1"
import json
import cv2
from pathlib import Path
from concurrent.futures import ProcessPoolExecutor
import numpy as np
import json
import math

directory_json = "D:/Manu/renders/UrbanSyns/UrbanSyns_BB2D/REST/boundingbox2D_edited"
files = []
COUNT = 0

for filename in os.listdir(directory_json):
    # Check if the file is a JSON
    if filename.endswith(".json"):
        files.append(filename)
def filter(json_file):
    json_path = os.path.join(directory_json, json_file)
    f = open(json_path)
    global COUNT
    # returns JSON object as
    # a dictionary
    data = json.load(f)

    # Searching the objects to filter
    # list
    bbox_dict = {}

    for i in data:
        if 'occlusion_percentage' in i:
            if i['occlusion_percentage'] < 0:

                print(json_file)
                print(i['occlusion_percentage'])
        else:
            print("No HAY")
            print(json_file)
            COUNT = COUNT + 1


    # Closing file
    f.close()


def main():
    for file in files:
        filter(file)

    # Create a ProcessPoolExecutor with a maximum of 8 threads
    #with ProcessPoolExecutor(max_workers=1) as executor:
     #   executor.map(filter, files)
    print(COUNT)


if __name__ == '__main__':
    main()
