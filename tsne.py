from sklearn.manifold import TSNE
from keras.datasets import mnist
from sklearn.datasets import load_iris
from numpy import reshape
import seaborn as sns
import pandas as pd
iris = load_iris()
x = iris.data
y = iris.target
tsne = TSNE(n_components=2, verbose=1, random_state=123)
z = tsne.fit_transform(x)