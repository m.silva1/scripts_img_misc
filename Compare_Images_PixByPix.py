import os

os.environ["OPENCV_IO_ENABLE_OPENEXR"] = "1"
import json
import cv2
import math


directory_image1 = "A:/users/cgl/workspace/Renders/UrbanSyns/Compare_SS/NoMedium"
directory_image2 = "A:/users/cgl/workspace/Renders/UrbanSyns/Compare_SS/Medium"

# Iterate through the all the instance maps
for filename in os.listdir(directory_image1):

    # Check if the file is an image
    if filename.endswith(".png"):

        # Opening Image1
        image1_path = os.path.join(directory_image1, filename)
        image1_img = cv2.imread(image1_path)

        # Opening Image2
        image2_path = os.path.join(directory_image2, filename)
        image2_img = cv2.imread(image2_path)

        # Get the dimensions of the image
        height, width, channels = image1_img.shape

        # Iterating through every pixel of the instance map
        for i in range(0, height):
            for j in range(0, width):  # ...
                if not ((image1_img[i, j] == image2_img[i, j]).all()):
                    print("No coinciden")
                    print(image1_img[i, j])
                    print(image2_img[i, j])



