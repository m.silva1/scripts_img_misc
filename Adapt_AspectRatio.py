import os

os.environ["OPENCV_IO_ENABLE_OPENEXR"] = "1"
import cv2
from concurrent.futures import ProcessPoolExecutor
import math

directory = "C:/Users/cgl/Documents/datasets/real/Mappilary/training/labels"
directory_out = "C:/Users/cgl/Documents/datasets/real/Mappilary/training/labels_aspect"

files = []
for filename in os.listdir(directory):
    # Check if the file is an image
    if filename.endswith(".png"):
        files.append(filename)


def adapt_aspect_ratio(image_param):
    image_path_out = os.path.join(directory_out, image_param)
    image_path_out = image_path_out.replace("//", "/")
    image_path = os.path.join(directory, image_param)
    image_path = image_path.replace("//", "/")
    image = cv2.imread(image_path)
    aspect_ratio = image.shape[1] / image.shape[0]
    if aspect_ratio < 1.5:
        newWidth = round(image.shape[1] / 1.777777777777778)
        cropSize = image.shape[0] - newWidth
        print(cropSize)
        if (cropSize % 2):
            image = image[0 + (round(cropSize / 2) + 1):image.shape[0] - round(cropSize / 2), 0:image.shape[1]]
        else:
            image = image[0 + (round(cropSize / 2)):image.shape[0] - round(cropSize / 2), 0:image.shape[1]]

    cv2.imwrite(image_path_out, image)


def main():
    # Create a ProcessPoolExecutor with a maximum of 8 threads
    with ProcessPoolExecutor(max_workers=24) as executor:
        executor.map(adapt_aspect_ratio, files)


if __name__ == '__main__':
    main()
